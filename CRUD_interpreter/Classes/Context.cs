﻿using System.Collections.Generic;
using CRUD_interpreter.Classes.Commands;
namespace CRUD_interpreter.Classes
{
	class Context
	{
		public CommandBase Result { get; set; } 

		public StorageType OutputType { get; set; }

		public List<string> Symbols { get;set; }

		public Context(List<string> _symbols)
		{
			OutputType = StorageType.Console;
			Symbols = _symbols;
		}
		public Context()
		{
			OutputType = StorageType.Console;
		}
		
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_interpreter.Classes
{
	struct DbEntity
	{
		string entity;
		string id;
		Dictionary<string, string> parameters;
	}
}

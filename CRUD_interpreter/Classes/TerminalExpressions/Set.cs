﻿using CRUD_interpreter.Classes.Commands;
using System;

namespace CRUD_interpreter.Classes.TerminalExpressions
{
	class Set : ExpressionBase
	{
		private string _what;
		private string _type;

		public Set(string Entity, string Output)
		{
			_what = Entity;
			_type = Output;
		}

		public override void Interpret(Context context)
		{
			context.Result = new IdleCommand();
			if (_what.ToLower() == "storage")
			{
				switch (_type.ToLower())
				{
					case "json":
						context.OutputType = StorageType.Json;
						break;
					case "console":
						context.OutputType = StorageType.Console;
						break;
					case "xml":
						context.OutputType = StorageType.Xml;
						break;
					case "sql":
						context.OutputType = StorageType.Sql;
						break;
					default:
						throw new InvalidOperationException($"Invalid storage type: {_type}");
				}
			}
			else
			{
				throw new InvalidOperationException($"Invalid symbol: {_what}");
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_interpreter.Classes.TerminalExpressions
{
	class UserId : ExpressionBase
	{
		private int Id { get; set; }

		public UserId(string input)
		{
			int tryId;
			if (int.TryParse(input, out tryId))
			{
				Id = tryId;
			}
			else
			{
				throw new InvalidOperationException($"Invalid symbol: {input}, not a integer.");
			}
		}

		public override void Interpret(Context context)
		{
			context.Result.Id = Id;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_interpreter.Classes.TerminalExpressions
{
	class Parameter : ExpressionBase
	{
		private string Key { get; set; }
		private string Value { get; set; }

		public Parameter(string input)
		{
			int separator = input.IndexOf(':');
			if (separator != -1)
			{
				Key = input.Substring(0, separator);
				Value = input.Substring(separator+1);
			}
			else
			{
				throw new InvalidOperationException($"Invalid parameter: {input}.");
			}
		}
		public override void Interpret(Context context)
		{
			context.Result.Parameters.Add(Key, Value);
		}
	}
}

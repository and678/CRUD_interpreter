﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_interpreter.Classes.TerminalExpressions
{
	class Entity : ExpressionBase
	{
		private string Name { get; set; }
		public Entity(string input)
		{
			Name = input;
		}
		public override void Interpret(Context context)
		{
			context.Result.Entity = Name;
		}
	}
}

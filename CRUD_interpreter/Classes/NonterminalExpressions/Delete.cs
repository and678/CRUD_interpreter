﻿using CRUD_interpreter.Classes.Commands;

namespace CRUD_interpreter.Classes.NonterminalExpressions
{
	class Delete : ExpressionBase
	{
		ExpressionBase ExpEntity { get; set; }
		ExpressionBase ExpUserId { get; set; }
		
		public Delete(ExpressionBase entity,
					ExpressionBase userId)
		{
			ExpEntity = entity;
			ExpUserId = userId;
		}
		public override void Interpret(Context context)
		{
			context.Result = new DeleteCommand(MakeWriter(context));
			ExpEntity.Interpret(context);
			ExpUserId.Interpret(context);
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRUD_interpreter.Classes.Commands;
namespace CRUD_interpreter.Classes.NonterminalExpressions
{
    class Add : ExpressionBase
    {
        ExpressionBase ExpEntity { get; set; }
		ExpressionBase ExpUserId { get; set; }
		List<ExpressionBase> ExpParameters { get; set; }

		public Add(ExpressionBase entity, 
					ExpressionBase userId, 
					List<ExpressionBase> parameters)
		{
			ExpEntity = entity;
			ExpUserId = userId;
			ExpParameters = parameters;
		}
		public override void Interpret(Context context)
        {
			context.Result = new AddCommand(MakeWriter(context));
			ExpEntity.Interpret(context);
			ExpUserId.Interpret(context);
			foreach (var par in ExpParameters)
			{
				par.Interpret(context);
			}
		}
    }
}

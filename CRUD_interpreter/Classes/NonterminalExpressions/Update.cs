﻿using CRUD_interpreter.Classes.Commands;
using System.Collections.Generic;

namespace CRUD_interpreter.Classes.NonterminalExpressions
{
	class Update : ExpressionBase
	{
		ExpressionBase ExpEntity { get; set; }
		ExpressionBase ExpUserId { get; set; }
		List<ExpressionBase> ExpParameters { get; set; }

		public Update(ExpressionBase entity,
					ExpressionBase userId,
					List<ExpressionBase> parameters)
		{
			ExpEntity = entity;
			ExpUserId = userId;
			ExpParameters = parameters;
		}
		public override void Interpret(Context context)
		{
			context.Result = new UpdateCommand(MakeWriter(context));
			ExpEntity.Interpret(context);
			ExpUserId.Interpret(context);
			foreach (var par in ExpParameters)
			{
				par.Interpret(context);
			}
		}
	}
}

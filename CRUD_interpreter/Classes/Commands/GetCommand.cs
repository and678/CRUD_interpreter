﻿using CRUD_interpreter.Classes.Writers;

namespace CRUD_interpreter.Classes.Commands
{
	class GetCommand : CommandBase
	{
		public GetCommand(IWriter _writer)
		{
			writer = _writer;
		}
		public override void Execute()
		{
			writer.Get(Entity, Id);
		}
	}
}

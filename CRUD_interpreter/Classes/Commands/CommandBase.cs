﻿using CRUD_interpreter.Classes.Writers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_interpreter.Classes.Commands
{
	abstract class CommandBase
	{
		public string Entity { get; set; }
		public int Id { get; set; }
		public Dictionary<string, string> Parameters { get; set; }


		protected IWriter writer;
		public abstract void Execute();

	}
}

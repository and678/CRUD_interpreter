﻿using CRUD_interpreter.Classes.Writers;
using System.Collections.Generic;


namespace CRUD_interpreter.Classes.Commands
{
	class AddCommand : CommandBase
	{
		public AddCommand(IWriter _writer)
		{
			Parameters = new Dictionary<string, string>();
			writer = _writer;
		}

		public override void Execute()
		{
			writer.Add(Entity, Id, Parameters);
		}
	}
}

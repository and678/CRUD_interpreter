﻿using CRUD_interpreter.Classes.Writers;

namespace CRUD_interpreter.Classes.Commands
{
	class DeleteCommand : CommandBase
	{
		public DeleteCommand(IWriter _writer)
		{
			writer = _writer;
		}
		public override void Execute()
		{
			writer.Delete(Entity, Id);
		}
	}
}

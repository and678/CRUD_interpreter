﻿using System.Collections.Generic;
using CRUD_interpreter.Classes.Writers;

namespace CRUD_interpreter.Classes.Commands
{
	class UpdateCommand : CommandBase
	{
		public UpdateCommand(IWriter _writer)
		{
			Parameters = new Dictionary<string, string>();
			writer = _writer;
		}
		public override void Execute()
		{
			writer.Update(Entity, Id, Parameters);
		}
	}
}

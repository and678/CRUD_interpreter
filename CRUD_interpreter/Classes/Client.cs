﻿using CRUD_interpreter.Classes.Commands;
using CRUD_interpreter.Classes.NonterminalExpressions;
using CRUD_interpreter.Classes.TerminalExpressions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CRUD_interpreter.Classes
{
    class Client
    {
		private Context mainContext;
		private Parser parser;

		public Client()
		{
			mainContext = new Context();
			parser = new Parser();
		}

		public CommandBase BuildAndInterpret(string input)
		{
			mainContext.Symbols = input.Split(' ').Select(s => s.Trim()).ToList();
			ExpressionBase mainExp = parser.BuildTree(mainContext);
			mainExp.Interpret(mainContext);
			return mainContext.Result;
        }

    }

	class Parser
	{
		public ExpressionBase BuildTree(Context context)
		{
			switch (context.Symbols[0].ToLower())
			{
				case "add":
					context.Symbols.RemoveAt(0);
					return BuildAdd(context);
				case "delete":
					context.Symbols.RemoveAt(0);
					return BuildDelete(context);
				case "update":
					context.Symbols.RemoveAt(0);
					return BuildUpdate(context);
				case "get":
					context.Symbols.RemoveAt(0);
					return BuildGet(context);
				case "set":
					context.Symbols.RemoveAt(0);
					return BuildSet(context);
				default:
					throw new InvalidOperationException($"Invalid symbol: {context.Symbols[0]}");
			}
		}

		private ExpressionBase BuildAdd(Context context)
		{
			if (context.Symbols.Count >= 3)
			{
				List<ExpressionBase> parameters = new List<ExpressionBase>();
				for (int i = 2; i < context.Symbols.Count; i++)
				{
					parameters.Add(new Parameter(context.Symbols[i]));
				}
				return new Add(new Entity(context.Symbols[0]), 
						new UserId(context.Symbols[1]),
						parameters);
			}
			throw new InvalidOperationException("Too few parameters");
		}


		private ExpressionBase BuildUpdate(Context context)
		{
			if (context.Symbols.Count >= 3)
			{
				List<ExpressionBase> parameters = new List<ExpressionBase>();
				for (int i = 2; i < context.Symbols.Count; i++)
				{
					parameters.Add(new Parameter(context.Symbols[i]));
				}
				return new Update(new Entity(context.Symbols[0]),
						new UserId(context.Symbols[1]),
						parameters);
			}
			throw new InvalidOperationException("Too few parameters");
		}

		private ExpressionBase BuildDelete(Context context)
		{
			if (context.Symbols.Count >= 2)
			{
				return new Delete(new Entity(context.Symbols[0]),
						new UserId(context.Symbols[1]));
			}
			throw new InvalidOperationException("Too few parameters");
		}

		private ExpressionBase BuildGet(Context context)
		{
			if (context.Symbols.Count >= 2)
			{
				return new Get(new Entity(context.Symbols[0]),
						new UserId(context.Symbols[1]));
			}
			throw new InvalidOperationException("Too few parameters");
		}

		private ExpressionBase BuildSet(Context context)
		{
			if (context.Symbols.Count >= 2)
			{
				return new Set(context.Symbols[0], context.Symbols[1]);
			}
			throw new InvalidOperationException("Too few parameters");
		}



	}


}

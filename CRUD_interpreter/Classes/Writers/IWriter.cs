﻿using System.Collections.Generic;

namespace CRUD_interpreter.Classes.Writers
{
	interface IWriter
	{
		void Add(string _Entity, int _UserId, Dictionary<string, string> _Parameters);
		void Delete(string _Entity, int _UserId);
		void Update(string _Entity, int _UserId, Dictionary<string, string> _Parameters);
		DbEntity Get(string _Entity, int _UserId);
	}
}

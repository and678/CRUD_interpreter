﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRUD_interpreter.Classes.Commands;

namespace CRUD_interpreter.Classes.Writers
{
	class ConsoleWriter : IWriter
	{
		public void Add(string _Entity, int _UserId, Dictionary<string, string> _Parameters)
		{
			Console.WriteLine($"Added {_Entity} with id: {_UserId.ToString()} and parameters: ");
			foreach (var a in _Parameters)
			{
				Console.WriteLine($"\t {a.Key}:{a.Value}");
			}
			Console.WriteLine();
		}

		public void Delete(string _Entity, int _UserId)
		{
			Console.WriteLine($"Deleted {_Entity} with id: {_UserId.ToString()}");
			Console.WriteLine();
		}

		public DbEntity Get(string _Entity, int _UserId)
		{
			Console.WriteLine($"Getting {_Entity} with id: {_UserId.ToString()}");
			Console.WriteLine();
			return new DbEntity();
		}

		public void Update(string _Entity, int _UserId, Dictionary<string, string> _Parameters)
		{
			Console.WriteLine($"Updated {_Entity} with id: {_UserId.ToString()} with parameters: ");
			foreach (var a in _Parameters)
			{
				Console.WriteLine($"\t {a.Key}:{a.Value}");
			}
			Console.WriteLine();
		}
	}
}

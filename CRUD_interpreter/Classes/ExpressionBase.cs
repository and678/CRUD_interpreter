﻿using System;
using CRUD_interpreter.Classes.Writers;

namespace CRUD_interpreter.Classes
{
	abstract class ExpressionBase
	{
		public abstract void Interpret(Context context);

		public IWriter MakeWriter(Context context)
		{
			switch (context.OutputType)
			{
				case StorageType.Json:
					return new JsonWriter();
				case StorageType.Xml:
					return new XmlWriter();
				case StorageType.Sql:
					throw new NotImplementedException();	//heheh
				case StorageType.Console:
					return new ConsoleWriter();
				default:
					throw new ArgumentException();
			}
		}
	}
}

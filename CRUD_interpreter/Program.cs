﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRUD_interpreter.Classes;

namespace CRUD_interpreter
{
    class Program
    {
        static void Main(string[] args)
        {
			Client CRUDClient = new Client();
			while (true)
			{
				try
				{
					Console.Write("\n>> ");
					CRUDClient.BuildAndInterpret(Console.ReadLine().Trim()).Execute();
				}
				catch (InvalidOperationException e)
				{
					Console.WriteLine(e.Message);
				}
			}
			
			

			Console.Read();

        }
    }
}
